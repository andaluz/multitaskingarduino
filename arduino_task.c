#include "arduino_task.h"
/**
 * ArduinoTask is a struct that contains necessary info to be processed
 * by run_looper (in arduino_looper.c)
 */

typedef int (*HANDLER)(); // typedef function pointer to handle a task

 struct ArduinoTask {
   int type_task;
   int priority;
   HANDLER handler;   // pass a handler like: handler = &turn_led_on
                      // than call it like: task->handler();
 }
