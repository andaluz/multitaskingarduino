#include "arduino_looper.h"
#include "arduino_task.h"
#include "simple_circular_queue.h"


// Maximum items in a queue is 10 for now.
#define       MAX_TASK_Q_SIZE       10

/**
* arduino_looper performs multitasking (not concurrently)
* You add ArduinoTask objects in a queue
* than a looper fetches the tasks from a queue one by one and
* processes them.
*
* If an interrupt occures, the ISR creates a task and adds it to the queue
* The looper checks if there's a task in the queue to process.
*/

ArduinoTask* task_queue[MAX_TASK_Q_SIZE];

void initialize_queue() {
  set_circular_queue(task_queue, MAX_TASK_Q_SIZE);
}

void add_new_task(ArduinoTask* task) {
  push_item_object(task);
}

ArduinoTask* get_next_task() {
  ArduinoTask* task = pop_item_object();
}

void process_task(ArduinoTask* task) {
    if(task) {
      task->handler();
    }
}

/**
* this function checks for new tasks to process
*/
void run_looper() {
  ArduinoTask *task = get_next_task();
  if(task) {
    process_task(task);
  } else {
    // no task found, go in idle mode, or something like that
    // i.e. turn on led, to indicate it has no task to process
  }
}
