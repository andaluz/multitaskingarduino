#include "simple_circular_queue.h"

void *main_queue = NULL;
int max_qu_len = 0;
int head = -1;
int tail = -1;
int num_of_items = 0;

void set_circular_queue(void *qu, int len) {
  if!qu) return;

  main_queue = qu;
  max_qu_len = len;
}

int is_qu_full() {
    if( (head == tail + 1) ||
      (head == 0 && tail == max_qu_len-1))
      return 1;

    return 0;
}

int is_qu_empty() {
    if(head == -1)
      return 1;
    else
      return 0;
}

void push_item_object(void *item) {
  if(is_qu_full()) {
    // we need to remove task in the head
    // this means the oldest unprocessed event has to be dropped/ignored,
    // because of too many events
    void* front = main_queue[head];
    if(front) free(front);  // remove this object

    // if head is on end of queue
    if(head == max_qu_len-1) {
      // tail takes over head, head is than moved cell
      tail = head;
      head = 0;
    } else {
      tail = head++;
    }
  } else {
    if(head == -1) head = 0;
    tail = (tail + 1) % max_qu_len;
    main_queue[tail] = item;
    num_of_tasks++;
  }
}

void* pop_item_object() {
  if(is_qu_empty()) {
    return NULL;
  } else {
    void* item = main_queue[head];
    if (head == tail){
        head = -1;
        tail = -1;
    } else {
        head = (head + 1) % max_qu_len;
    }
    num_of_tasks--;

    return item;
  }
}
